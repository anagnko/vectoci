# CI scripts for VECTO project

## SAST scanning

Contains AutoDevOps CI pipeline-code `./legent-sast.yml` to scan sources for:

- [secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#edit-the-gitlab-ciyml-file-manually)
- [Static analysis](https://docs.gitlab.com/ee/user/application_security/sast/index.html)

Use it if no other CI pipeline is available.
